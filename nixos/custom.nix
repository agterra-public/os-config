{ config, pkgs, ... }:

{
  virtualisation = {
    docker.enable = true;
  };

  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #  wget
    vim
    vscode
    docker-compose
    git
    gnumake
    _1password-gui
    kitty
    rofi
    waybar
  ];
  
  programs = {
    hyprland.enable = true;
  };
}
